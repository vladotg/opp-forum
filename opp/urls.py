# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include, url

from forum.views import HomeView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'styriacms.views.home', name='home'),
    # url(r'^styriacms/', include('styriacms.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Molim da se moduli poslažu po abecedi kako bi se lakše snašli

    url(r'^forum$', HomeView.as_view(), name="home"),
    # url(r'^forum/(?P<post_id>\d+)$', PostView.as_view(), name="post"),

)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT})
    )
