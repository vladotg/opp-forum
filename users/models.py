# -*- coding: utf-8 -*-

from django.contrib.auth.models import User

from django.db import models
from django.utils.translation import ugettext as _


USER_GENDER = (
    ('F', _(u'Ženski')),
    ('M', _(u'Muški'))
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name=_(u'Korisnik'),
        related_name='profile')
    avatar = models.ImageField(verbose_name=_(u"Avatar"), null=True, blank=True)

    can_comment = models.BooleanField(verbose_name=_(u'Može li komentirati'),
        default=True)
    
    send_newsletter = models.BooleanField(verbose_name=_(u'Šalji newsletter'),
        default=True)

    gender = models.CharField(verbose_name=_(u'Spol'), choices=USER_GENDER,
        max_length=1, default='N')

    date_of_birth = models.DateField(verbose_name=_(u'Datum rođenja'),
        blank=True, null=True)
    address_street = models.CharField(verbose_name=_(u'Ulica'), max_length=150,
        blank=True, null=True)
    address_place = models.CharField(verbose_name=_(u'Mjesto'), max_length=150,
        blank=True, null=True)
    address_postcode = models.CharField(verbose_name=_(u'Poštanski broj'),
        max_length=10, blank=True, null=True)
    address_country = models.CharField(verbose_name=_(u'Država'), max_length=40,
        blank=True, null=True)
    country_code = models.CharField(verbose_name=_(u'Kod države'), max_length=2,
        blank=True, null=True)

    activated = models.BooleanField(verbose_name=_(u'Aktiviran račun'),
        default=False)
    activated_date = models.DateTimeField(
        verbose_name=_(u'Vrijeme aktiviranja'), null=True, blank=True)
    activation_token = models.CharField(
        verbose_name=_(u'Token za aktivaciju računa'),
        max_length=32, default='', blank=True)
    activation_email_sent = models.DateTimeField(
        verbose_name=_(u'Vrijeme slanja aktivacijskog maila'),
        blank=True, null=True)


    def __unicode__(self):
        return unicode(self.user.username)

    class Meta:
        verbose_name = _(u'Korisnički profil')
        verbose_name_plural = _(u'Korisnički profili')